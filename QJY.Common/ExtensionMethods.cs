﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace QJY.Common
{
    public static class MyExtensions
    {
        public static int[] SplitTOInt(this string strs, char ch)
        {
            if (strs == "")
            {
                int[] arrint = new int[0];
                return arrint;

            }
            else
            {
                string[] arrstr = strs.Split(ch);
                int[] arrint = new int[arrstr.Length];
                for (int i = 0; i < arrstr.Length; i++)
                {
                    arrint[i] = int.Parse(arrstr[i].ToString());
                }
                return arrint;
            }
        }



        public static List<int> SplitTOIntList(this string strs, char ch)
        {
            List<int> Lstr = new List<int>();
            string[] arrstr = strs.Split(ch);
            int[] arrint = new int[arrstr.Length];
            for (int i = 0; i < arrstr.Length; i++)
            {
                Lstr.Add(int.Parse(arrstr[i].ToString()));
            }
            return Lstr;
        }

        public static List<string> SplitTOList(this string strs, char ch)
        {
            List<string> Lstr = new List<string>();
            string[] arrstr = strs.Split(ch);
            int[] arrint = new int[arrstr.Length];
            for (int i = 0; i < arrstr.Length; i++)
            {
                Lstr.Add(arrstr[i].ToString());
            }
            return Lstr;
        }

        public static Dictionary<string, string> SplitTODictionary(this string strs, char ch, string strKey)
        {
            Dictionary<string, string> Lstr = new Dictionary<string, string>();
            string[] arrstr = strs.Split(ch);
            int[] arrint = new int[arrstr.Length];
            for (int i = 0; i < arrstr.Length; i++)
            {
                Lstr.Add(arrstr[i].ToString(), strKey);
            }
            return Lstr;
        }

        /// <summary>
        /// 将List转化为String
        /// </summary>
        /// <param name="Lists"></param>
        /// <param name="ch"></param>
        /// <returns></returns>
        public static string ListTOString(this List<string> Lists, char ch)
        {
            string strReturn = "";
            foreach (var item in Lists)
            {
                strReturn = strReturn + item.ToString() + ch;
            }
            return strReturn.TrimEnd(ch);
        }

        /// <summary>
        /// 将List(int)转化为String
        /// </summary>
        /// <param name="Lists"></param>
        /// <param name="ch"></param>
        /// <returns></returns>
        public static string ListTOString(this List<int> Lists, char ch)
        {
            string strReturn = "";
            foreach (var item in Lists)
            {
                strReturn = strReturn + item.ToString() + ch;
            }
            return strReturn.TrimEnd(ch);
        }
        /// <summary>
        /// 获取需要IN的格式
        /// </summary>
        /// <param name="strKys"></param>
        /// <returns></returns>
        public static string ToFormatLike(this string strKys)
        {
            StringBuilder sbKeys = new StringBuilder();
            foreach (var item in strKys.Split(','))
            {
                sbKeys.AppendFormat("'" + item.ToString().TrimEnd() + "',");
            }
            return sbKeys.Length > 0 ? sbKeys.ToString().TrimEnd(',').Trim('\'') : "";
        }



        /// <summary>
        /// 获取需要IN的格式
        /// </summary>
        /// <param name="strKys"></param>
        /// <returns></returns>
        public static string ToFormatLike(this string strKys, char ch)
        {
            StringBuilder sbKeys = new StringBuilder();
            strKys = strKys.TrimEnd(ch);
            foreach (var item in strKys.Split(ch))
            {
                sbKeys.AppendFormat("'" + item.ToString() + "',");
            }
            return sbKeys.Length > 0 ? sbKeys.ToString().TrimEnd(',').Trim('\'') : "";
        }




        /// <summary>
        /// 取前N个字符,后面的用省略号代替
        /// </summary>
        /// <param name="strKys"></param>
        /// <returns></returns>
        public static string ToMangneStr(this string strKys, int intLenght)
        {

            return strKys.Length > intLenght ? strKys.Substring(0, intLenght) + "…………" : strKys;
        }



        public static DataTable ToDataTable<T>(this IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            // column names
            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }

        public static DataTable OrderBy(this DataTable dt, string orderBy)
        {
            dt.DefaultView.Sort = orderBy;
            return dt.DefaultView.ToTable();
        }
        public static DataTable Where(this DataTable dt, string where)
        {
            DataTable resultDt = dt.Clone();
            DataRow[] resultRows = dt.Select(where);
            foreach (DataRow dr in resultRows) resultDt.Rows.Add(dr.ItemArray);
            return resultDt;
        }


        /// <summary>
        /// Datatable转换为Json
        /// </summary>
        /// <param name="table">Datatable对象</param>
        /// <returns>Json字符串</returns>
        public static string ToJson(this DataTable dt)
        {
            StringBuilder jsonString = new StringBuilder();
            jsonString.Append("[");
            DataRowCollection drc = dt.Rows;
            for (int i = 0; i < drc.Count; i++)
            {
                jsonString.Append("{");
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    string strKey = dt.Columns[j].ColumnName;
                    string strValue = drc[i][j].ToString();
                    Type type = dt.Columns[j].DataType;
                    jsonString.Append("\"" + strKey + "\":");
                    strValue = StringFormat(strValue, type);
                    if (j < dt.Columns.Count - 1)
                    {
                        jsonString.Append(strValue + ",");
                    }
                    else
                    {
                        jsonString.Append(strValue);
                    }
                }
                jsonString.Append("},");
            }
            jsonString.Remove(jsonString.Length - 1, 1);
            if (jsonString.Length != 0)
            {
                jsonString.Append("]");
            }
            return jsonString.ToString();
        }



        public static int ToInt32(this Object obj)
        {
            return Convert.ToInt32(obj);
        }


        public static DateTime ToDateTime(this Object obj)
        {
            return Convert.ToDateTime(obj);
        }

        public static T GetProperty<T>(this object obj, string propertyName)
        {
            var property = obj.GetType().GetProperty(propertyName);
            if (property != null)
            {
                return (T)property.GetValue(obj, null);
            }
            throw new ArgumentNullException(propertyName);
        }



        /// <summary>
        /// DataTable转成Json
        /// </summary>
        /// <param name="jsonName"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToJson(this DataTable dt, string jsonName)
        {
            StringBuilder Json = new StringBuilder();
            if (string.IsNullOrEmpty(jsonName))
                jsonName = dt.TableName;
            Json.Append("{\"" + jsonName + "\":[");
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    Json.Append("{");
                    for (int j = 0; j < dt.Columns.Count; j++)
                    {
                        Type type = dt.Rows[i][j].GetType();
                        Json.Append("\"" + dt.Columns[j].ColumnName.ToString() + "\":" + StringFormat(dt.Rows[i][j].ToString(), type));
                        if (j < dt.Columns.Count - 1)
                        {
                            Json.Append(",");
                        }
                    }
                    Json.Append("}");
                    if (i < dt.Rows.Count - 1)
                    {
                        Json.Append(",");
                    }
                }
            }
            Json.Append("]}");
            return Json.ToString();
        }

        /// <summary>
        /// 过滤特殊字符
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string String2Json(String s)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                char c = s.ToCharArray()[i];
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\""); break;
                    case '\\':
                        sb.Append("\\\\"); break;
                    case '/':
                        sb.Append("\\/"); break;
                    case '\b':
                        sb.Append("\\b"); break;
                    case '\f':
                        sb.Append("\\f"); break;
                    case '\n':
                        sb.Append("\\n"); break;
                    case '\r':
                        sb.Append("\\r"); break;
                    case '\t':
                        sb.Append("\\t"); break;
                    default:
                        sb.Append(c); break;
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// 格式化字符型、日期型、布尔型
        /// </summary>
        /// <param name="str"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string StringFormat(string str, Type type)
        {
            if (type == typeof(string))
            {
                str = String2Json(str);
                str = "\"" + str + "\"";
            }
            else if (type == typeof(DateTime))
            {
                str = "\"" + str + "\"";
            }
            else if (type == typeof(Int32))
            {
                if (str.Trim() == "")
                {
                    str = "\"" + str + "\"";
                }
            }
            else if (type == typeof(bool))
            {
                str = str.ToLower();
            }
            return str;
        }



        /// <summary>
        /// 过滤特殊字符
        /// 如果字符串为空，直接返回。
        /// </summary>
        /// <param name="str">需要过滤的字符串</param>
        /// <returns>过滤好的字符串</returns>
        public static string FilterSpecial(this string str)
        {
            if (str == "")
            {
                return str;
            }
            else
            {
                //str = str.Replace("'", "");
                //str = str.Replace("<", "");
                //str = str.Replace(">", "");
                //str = str.Replace("%", "");
                str = str.Replace("'delete", "");
                str = str.Replace("'truncate", "");
                str = str.Replace("''", "");
                //str = str.Replace("\"\"", "");
                //str = str.Replace(",", "");
                //str = str.Replace(".", "");
                //str = str.Replace(">=", "");
                //str = str.Replace("=<", "");
                //str = str.Replace(";", "");
                //str = str.Replace("||", "");
                //str = str.Replace("[", "");
                //str = str.Replace("]", "");
                //str = str.Replace("&", "");
                //str = str.Replace("#", "");
                //str = str.Replace("/", "");
                //str = str.Replace("|", "");
                //str = str.Replace("?", "");
                //str = str.Replace(">?", "");
                //str = str.Replace("?<", "");
                return str;
            }
        }



        /// <summary>
        /// 防止SQL注入
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        public static string FormPar(this string html)
        {
            System.Text.RegularExpressions.Regex regex1 = new System.Text.RegularExpressions.Regex(@"<script[\s\S]+</script *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex2 = new System.Text.RegularExpressions.Regex(@" href *= *[\s\S]*script *:", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex3 = new System.Text.RegularExpressions.Regex(@" on[\s\S]*=", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex4 = new System.Text.RegularExpressions.Regex(@"<iframe[\s\S]+</iframe *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex5 = new System.Text.RegularExpressions.Regex(@"<frameset[\s\S]+</frameset *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex10 = new System.Text.RegularExpressions.Regex(@"select", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex11 = new System.Text.RegularExpressions.Regex(@"update", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex12 = new System.Text.RegularExpressions.Regex(@"delete", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            html = regex1.Replace(html, ""); //过滤<script></script>标记
            html = regex2.Replace(html, ""); //过滤href=javascript: (<A>) 属性
            html = regex3.Replace(html, " _disibledevent="); //过滤其它控件的on...事件
            html = regex4.Replace(html, ""); //过滤iframe
            html = regex10.Replace(html, "s_elect");
            html = regex11.Replace(html, "u_pudate");
            html = regex12.Replace(html, "d_elete");
            html = html.Replace("'", "’");
            html = html.Replace(" ", " ");
            return html;
        }


        /// <summary>
        /// 删除列
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sCol">保存的列,其他删除</param>
        /// <returns></returns>
        public static DataTable DelTableCol(this DataTable dt, string sCol)
        {

            if (sCol == "")
                return dt;
            string[] sp = sCol.Split(',');
            string sqldd = "";
            for (int n = 0; n < sp.Length; n++)
            {
                sqldd += sp[n].Split('|')[0] + ',';
            }
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if (!sqldd.Split(',').Contains(dt.Columns[i].ColumnName))
                {
                    dt.Columns.RemoveAt(i);
                    i = i - 1;
                }
                else
                {
                    for (int j = 0; j < sp.Length; j++)
                    {
                        string[] sg = sp[j].Split('|');
                        if (sg.Length > 1 && sg[0] == dt.Columns[i].ColumnName)
                        {
                            if (sg.Length == 1)
                            {
                                dt.Columns[i].ColumnName = sg[1];

                            }
                        }
                    }
                }
            }
            return dt;
        }

        /// <summary>   
        /// 根据条件过滤表   
        /// </summary>   
        /// <param name="dt">未过滤之前的表</param>   
        /// <param name="filter">过滤条件</param>   
        /// <returns>返回过滤后的表</returns>   
        public static DataTable FilterTable(this DataTable dt, string filter, string isSJ = "N")
        {

            DataTable newTable = dt.Clone();
            DataRow[] drs = dt.Select(filter);
            foreach (DataRow dr in drs)
            {
                newTable.Rows.Add(dr.ItemArray);
            }

            return newTable;
        }


        /// <summary>
        /// 随机排序
        /// </summary>
        /// <param name="newTable"></param>
        /// <returns></returns>
        public static DataTable SJTable(this DataTable newTable)
        {

            Random ran = new Random();
            newTable.Columns.Add("sort", typeof(int));
            for (int i = 0; i < newTable.Rows.Count; i++)
            {
                newTable.Rows[i]["sort"] = ran.Next(0, 100);
            }
            DataView dv = newTable.DefaultView;
            dv.Sort = "sort asc";
            newTable = dv.ToTable();
            return newTable;
        }


        //替换datatable的NULL
        public static DataTable FilterNull(this DataTable newTable)
        {


            for (int i = 0; i < newTable.Rows.Count; i++)
            {
                for (int m = 0; m < newTable.Columns.Count; m++)
                {
                    if (newTable.Rows[i][m] == null)
                    {
                        newTable.Rows[i][m] = "";
                    }
                }
            }
            return newTable;
        }



        /// <summary>
        /// 处理生成的DataTable
        /// </summary>
        /// <param name="newTable"></param>
        /// <param name="addclName">添加的列</param>
        /// <param name="clNames">需要合并的列</param>
        /// <returns></returns>
        public static DataTable AddColum(this DataTable DTTable, string addclName, char strChar, params string[] clNames)
        {

            DTTable.Columns.Add(addclName);
            for (int i = 0; i < DTTable.Rows.Count; i++)
            {
                string strTemp = "";
                for (int m = 0; m < clNames.Length; m++)
                {
                    strTemp = strTemp + DTTable.Rows[i][clNames[m]].ToString() + strChar;
                }
                DTTable.Rows[i][addclName] = strTemp.TrimEnd(strChar);

            }

            return DTTable;
        }

        /// <summary>
        /// dataTable分页
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public static DataTable SplitDataTable(this DataTable dt, int PageIndex, int PageSize)
        {
            if (PageIndex == 0)
                return dt;
            DataTable newdt = dt.Clone();
            //newdt.Clear();
            int rowbegin = (PageIndex - 1) * PageSize;
            int rowend = PageIndex * PageSize;

            if (rowbegin >= dt.Rows.Count)
                return newdt;

            if (rowend > dt.Rows.Count)
                rowend = dt.Rows.Count;
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                DataRow newdr = newdt.NewRow();
                DataRow dr = dt.Rows[i];
                foreach (DataColumn column in dt.Columns)
                {
                    newdr[column.ColumnName] = dr[column.ColumnName];
                }
                newdt.Rows.Add(newdr);
            }

            return newdt;
        }




        public static DataTable EMToDataTable<T>(this IEnumerable<T> varlist) where T : class, new()
        {
            //定义要返回的DataTable对象
            DataTable dtReturn = new DataTable();
            //安全性检查
            if (varlist == null)
            {
                return dtReturn;
            }
            //保存列集合的属性信息数组
            PropertyInfo[] oProps = typeof(T).GetProperties();
            //循环PropertyInfo数组
            foreach (PropertyInfo pi in oProps)
            {
                //得到属性的类型
                Type colType = pi.PropertyType;
                //如果属性为泛型类型
                if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    //获取泛型类型的参数
                    colType = colType.GetGenericArguments()[0];
                }
                //将类型的属性名称与属性类型作为DataTable的列数据
                dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
            }
            //循环遍历集合，使用反射获取类型的属性信息
            foreach (T rec in varlist)
            {
                //新建一个用于添加到DataTable中的DataRow对象
                DataRow dr = dtReturn.NewRow();
                //循环遍历属性集合
                foreach (PropertyInfo pi in oProps)
                {
                    //为DataRow中的指定列赋值
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue(rec, null);
                }
                //将具有结果值的DataRow添加到DataTable集合中
                dtReturn.Rows.Add(dr);
            }
            //返回DataTable对象
            return dtReturn;
        }



        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="colName"></param>
        /// <returns></returns>
        public static string GetDTColum(this DataTable dt, string colName, string strDefault = "")
        {
            string str = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                str = str + dt.Rows[i][colName].ToString().Trim() + ",";
            }
            if (str == "")
            {
                return strDefault;
            }
            else
            {

                return str.Trim().SplitTOList(',').Distinct().ToList().ListTOString(',');
            }
        }

        /// <summary>
        /// 行转列
        /// </summary>
        /// <param name="tableData"></param>
        /// <returns></returns>
        public static DataTable SwapTable(this DataTable tableData)
        {
            //获取table的行
            int intRows = tableData.Rows.Count;
            //获取table的列
            int intColumns = tableData.Columns.Count;

            //转二维数组
            string[,] arrayData = new string[intRows, intColumns];
            for (int i = 0; i < intRows; i++)
            {
                for (int j = 0; j < intColumns; j++)
                {
                    arrayData[i, j] = tableData.Rows[i][j].ToString();
                }
            }
            //下标对换
            string[,] arrSwap = new string[intColumns, intRows];
            for (int m = 0; m < intColumns; m++)
            {
                for (int n = 0; n < intRows; n++)
                {
                    arrSwap[m, n] = arrayData[n, m];
                }
            }
            DataTable dt = new DataTable();
            //添加列
            for (int k = 0; k < intRows; k++)
            {
                dt.Columns.Add(
                        new DataColumn(arrSwap[0, k])
                    );
            }
            //添加行
            for (int r = 1; r < intColumns; r++)
            {
                DataRow dr = dt.NewRow();
                for (int c = 0; c < intRows; c++)
                {
                    dr[c] = arrSwap[r, c].ToString();
                }
                dt.Rows.Add(dr);
            }
            //添加行头
            DataColumn ColRowHead = new DataColumn(tableData.Columns[0].ColumnName);
            dt.Columns.Add(ColRowHead);
            dt.Columns[ColRowHead.ColumnName].SetOrdinal(0);
            for (int i = 0; i < intColumns - 1; i++)
            {
                dt.Rows[i][ColRowHead.ColumnName] = tableData.Columns[i + 1].ColumnName;
            }
            return dt;
        }

        /// <summary>
        /// 添加转换列
        /// </summary>
        /// <param name="tableData"></param>
        /// <param name="strColName">当前表的查询字段</param>
        /// <param name="dtData">数据Table</param>
        /// <param name="straddColName">数据表的查询字段</param>
        /// <param name="straddColVal">要添加的列</param>
        /// <returns></returns>
        public static DataTable addColName(this DataTable tableData, string strColName, DataTable dtData, string straddColName, string straddColVal)
        {
            if (!tableData.Columns.Contains(straddColVal))
            {
                tableData.Columns.Add(straddColVal);
            }
            for (int i = 0; i < tableData.Rows.Count; i++)
            {
                string strTemp = tableData.Rows[i][strColName].ToString();
                DataTable dtTemp = dtData.Where(" " + straddColName + "='" + strTemp + "' ");
                if (dtTemp.Rows.Count > 0)
                {
                    tableData.Rows[i][straddColVal] = dtTemp.Rows[0][straddColVal].ToString();
                }
            }
            return tableData;

        }
    }

}
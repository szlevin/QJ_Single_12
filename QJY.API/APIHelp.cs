﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using QJY.Common;

namespace QJY.API
{
    class APIHelp
    {
    }


    public static class APIExtensions
    {
        public static string Request(this JObject JData, string strPro, string strDefault = null)
        {

            return JData[strPro] != null ? JData[strPro].ToString(): strDefault;

            ////转一下，不然JOBJECT认大小写
            //Dictionary<string, object> d = new Dictionary<string, object>(JData.ToObject<IDictionary<string, object>>(), StringComparer.CurrentCultureIgnoreCase);
            //if (d.ContainsKey(strPro) && d[strPro]!=null)
            //{
            //    return d[strPro].ToString();
            //}
            //else
            //{
            //    return strDefault;
            //}
        }
    }
}

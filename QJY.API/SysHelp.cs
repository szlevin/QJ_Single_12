﻿using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QJY.API
{
    class SysHelp
    {

        /// <summary>
        /// 教务缓存管理
        /// </summary>
        public class SysCathe
        {

            public class SysKeys
            {

                /// <summary>
                /// 字典
                /// </summary>
                public static string zd = "zidian";

               
                /// <summary>
                /// 系部
                /// </summary>
                public static string xb = "bm";

             

                /// <summary>
                /// 教师数据
                /// </summary>
                public static string users = "sysusers";



              
            }

            /// <summary>
            /// 清理Key
            /// </summary>
            /// <param name="Key"></param>
            public static void ClearCathe(string Key)
            {
                CacheHelp.Remove(Key);
            }




            /// <summary>
            /// 系部缓存数据
            /// </summary>
            /// <returns></returns>
            public static DataTable XBList()
            {
                DataTable ListData = CacheHelp.Get(SysCathe.SysKeys.xb) as DataTable;
                if (ListData != null)
                {
                    return ListData;
                }
                else
                {
                    ListData = new JH_Auth_BranchB().GetDTByCommand(" select RoomCode as xbdm ,DeptCode,DeptName as xbmc,DeptDesc as xbjc, isHasQX as jxxb from jh_auth_branch ");
                    CacheHelp.Set(SysCathe.SysKeys.xb, ListData);
                    return ListData;
                }
            }




            /// <summary>
            /// 获取字典数据
            /// </summary>
            /// <returns></returns>
            public static List<JH_Auth_ZiDian> ZDList()
            {
                List<JH_Auth_ZiDian> ListData = CacheHelp.Get(SysCathe.SysKeys.zd) as List<JH_Auth_ZiDian>;
                if (ListData != null)
                {
                    return ListData;
                }
                else
                {
                    ListData = new JH_Auth_ZiDianB().GetALLEntities().ToList();
                    CacheHelp.Set(SysCathe.SysKeys.zd, ListData);
                    return ListData;
                }
            }





            




            /// <summary>
            /// 用户缓存数据
            /// </summary>
            /// <returns></returns>
            public static DataTable YHList()
            {
                DataTable ListData = CacheHelp.Get(SysCathe.SysKeys.users) as DataTable;
                if (ListData != null)
                {
                    return ListData;
                }
                else
                {
                    ListData = new JH_Auth_UserB().GetALLEntities().Select(it => new { UserName = it.UserName, UserRealName = it.UserRealName }).ToDataTable();
                    CacheHelp.Set(SysCathe.SysKeys.users, ListData);
                    return ListData;
                }
            }

         



        }


        /// <summary>
        /// 转换类
        /// </summary>
        public class SysCovert
        {



         


            /// <summary>
            /// 工号转姓名
            /// </summary>
            /// <param name="strBJ"></param>
            /// <returns></returns>
            public static string CovYHDM_NAME(string strDM)
            {
                DataTable dtTemp = SysCathe.YHList().Where(" UserName in ('" + strDM.ToFormatLike() + "') ");
                return dtTemp.GetDTColum("UserRealName", strDM);
            }



            /// <summary>
            /// 姓名转工号
            /// </summary>
            /// <param name="strBJ"></param>
            /// <returns></returns>
            public static string CovName_YHNAME(string strYHName)
            {
                DataTable dtTemp = SysCathe.YHList().Where(" UserRealName  in ('" + strYHName.ToFormatLike() + "') ");
                return dtTemp.GetDTColum("UserName", strYHName);
            }

            /// <summary>
            /// 转化获取字典
            /// </summary>
            /// <param name="strTypeNO"></param>
            /// <param name="strClass"></param>
            /// <returns></returns>
            public static string CovZDTypeName(string strTypeNO, string strClass)
            {
                DataTable dtTemp = new DataTable();
                dtTemp = SysCathe.ZDList().Where(D => D.Class == strClass && D.TypeNO == strTypeNO).ToDataTable();
                return dtTemp.GetDTColum("TypeName", strTypeNO);
            }



            /// <summary>
            /// 机构代码转名称
            /// </summary>
            /// <param name="strJGDM"></param>
            /// <returns></returns>
            public static string CovJGDM_JGNAME(string strJGDM)
            {
                DataTable dtTemp = SysCathe.XBList().Where(" DeptCode in ('" + strJGDM.ToFormatLike() + "') ");
                return dtTemp.GetDTColum("XBMC", strJGDM);
            }


            /// <summary>
            /// 机构代码转名称
            /// </summary>
            /// <param name="strJGDM"></param>
            /// <returns></returns>
            public static string CovJGDM_JGJC(string strJGDM)
            {
                DataTable dtTemp = SysCathe.XBList().Where(" DeptCode in ('" + strJGDM.ToFormatLike() + "') ");
                return dtTemp.GetDTColum("xbjc", strJGDM);
            }

        }


        /// <summary>
        /// 检查类
        /// </summary>

        public class SysCheck
        {
            #region 检查方法

            /// <summary>
            /// 检查系部名称是否存在
            /// </summary>
            /// <param name="strXBMC"></param>
            /// <returns></returns>
            public static bool isExistXB(string strXBMC)
            {
                bool isExit = false;
                var dtData = new JH_Auth_BranchB().GetEntities(D => (D.DeptName == strXBMC || D.DeptDesc == strXBMC));
                if (dtData.Count() > 0)
                {
                    isExit = true;
                }
                return isExit;
            }
            /// <summary>
            /// 检查字典类别
            /// </summary>
            /// <param name="strClass"></param>
            /// <param name="strZDVal"></param>
            /// <returns></returns>
            public static bool isExistZDClass(string strClass)
            {
                bool isExit = false;
                var dtData = SysHelp.SysCathe.ZDList().Where(D => (D.Class == strClass));
                if (dtData.Count() > 0)
                {
                    isExit = true;
                }
                return isExit;

            }

            /// <summary>
            /// 检查字典数据
            /// </summary>
            /// <param name="strClass"></param>
            /// <param name="strZDVal"></param>
            /// <returns></returns>
            public static bool isExistZD(string strClass, string strZDVal)
            {
                bool isExit = false;
                var dtData = SysHelp.SysCathe.ZDList().Where(D => (D.Class == strClass && D.TypeName == strZDVal));
                if (dtData.Count() > 0)
                {
                    isExit = true;
                }
                return isExit;

            }

            #endregion
        }

    }
}

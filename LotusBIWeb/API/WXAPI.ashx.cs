﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using QJY.API;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace QJY.WEB
{
    /// <summary>
    /// WXAPI 的摘要说明
    /// </summary>
    public class WXAPI : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.AddHeader("Access-Control-Allow-Origin", "*");
            context.Response.AddHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS,DELETE"); //支持的http 动作
            context.Response.AddHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type,authorization");
            context.Response.AddHeader("Access-Control-Allow-Credentials", "true");
            context.Response.AddHeader("pragma", "no-cache");
            context.Response.AddHeader("cache-control", "");
            context.Response.CacheControl = "no-cache";
            string strAction = context.Request["Action"] ?? "";
            Msg_Result Model = new Msg_Result() { Action = strAction.ToUpper(), ErrorMsg = "" };
            JH_Auth_QYB DBCon = new JH_Auth_QYB();

            if (!string.IsNullOrEmpty(strAction))
            {



                #region 获取唯一code
                if (strAction.ToUpper() == "GetUserCodeByCode".ToUpper())
                {
                    #region 获取Code
                    Model.ErrorMsg = "获取Code错误，请重试";

                    string strCode = context.Request["code"] ?? "";
                    string strCorpID = context.Request["corpid"] ?? "";
                    string strModelCode = context.Request["funcode"] ?? "";

                    if (!string.IsNullOrEmpty(strCode))
                    {

                        var qy = new JH_Auth_QYB().GetEntity(p => p.corpId == strCorpID);
                        if (qy != null)
                        {
                            try
                            {

                                //通过微信接口获取用户名
                                WXHelp wx = new WXHelp(qy);
                                string username = wx.GetUserDataByCode(strCode, strModelCode);
                                if (!string.IsNullOrEmpty(username))
                                {
                                    var jau = new JH_Auth_UserB().GetUserByUserName(qy.ComId, username);
                                    if (jau != null)
                                    {
                                        //如果PCCode为空或者超过60分钟没操作,统统重新生成PCCode,并更新最新操作时间
                                        if (jau.logindate == null)
                                        {
                                            jau.logindate = DateTime.Now;
                                        }
                                        TimeSpan ts = new TimeSpan(jau.logindate.Value.Ticks).Subtract(new TimeSpan(DateTime.Now.Ticks)).Duration();
                                        if (string.IsNullOrEmpty(jau.pccode) || ts.TotalMinutes > 60)
                                        {
                                            string strGuid = JwtHelper.CreateJWT(jau.UserName).Token;
                                            jau.pccode = strGuid;
                                            jau.logindate = DateTime.Now;
                                            new JH_Auth_UserB().Update(jau);
                                        }
                                        Model.ErrorMsg = "";
                                        Model.Result = jau.pccode;
                                        Model.Result1 = jau.UserName;
                                        Model.Result2 = ts.TotalMinutes;
                                        Model.Result3 = qy.FileServerUrl;
                                    }

                                }
                                else
                                {
                                    Model.ErrorMsg = "当前用户不存在";
                                }
                            }
                            catch (Exception ex)
                            {
                                Model.ErrorMsg = ex.ToString();
                            }
                        }
                        else
                        {
                            Model.ErrorMsg = "当前企业号未在电脑端注册";
                        }

                    }
                    else
                    {
                        Model.ErrorMsg = "Code为空";
                    }
                    #endregion
                }
                #endregion
                #region 是否存在
                if (strAction.ToUpper() == "isexist".ToUpper())
                {
                    if (context.Request["szhlcode"] != null)
                    {
                        //通过Cookies获取Code
                        //string szhlcode = "5ab470be-4988-4bb3-9658-050481b98fca"; 
                        string szhlcode = context.Request["szhlcode"].ToString();
                        //通过Code获取用户名，然后执行接口方法
                        var jau = new JH_Auth_UserB().GetUserByPCCode(szhlcode);
                        if (jau == null)
                        {
                            Model.Result = "NOCODE";
                        }
                    }
                }
                #endregion
                #region 发送提醒
                if (strAction.ToUpper() == "AUTOALERT")
                {
                    TXSXAPI.AUTOALERT();
                }

                if (strAction.ToUpper() == "BINDYH")//绑定用户
                {

                    string password = context.Request["password"] ?? "";
                    string username = context.Request["UserName"] ?? "";
                    string wxopenid = context.Request["wxopenid"] ?? "";
                    string nickname = context.Request["nickname"] ?? "";
                    string txurl = context.Request["txurl"] ?? "";


                    JH_Auth_QY qyModel = new JH_Auth_QYB().GetALLEntities().First();
                    password = CommonHelp.GetMD5(password);
                    JH_Auth_User userInfo = new JH_Auth_User();

                    List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => (d.UserName == username || d.mobphone == username) && d.UserPass == password).ToList();
                    if (userList.Count() == 0)
                    {
                        Model.ErrorMsg = "用户名或密码不正确";
                    }
                    else
                    {
                        userInfo = userList[0];
                        if (userInfo.IsUse != "Y")
                        {
                            Model.ErrorMsg = "用户被禁用,请联系管理员";
                        }
                        if (Model.ErrorMsg == "")
                        {
                            userInfo.weixinCard = wxopenid;
                            userInfo.NickName = nickname;
                            userInfo.txurl = txurl;
                            new JH_Auth_UserB().Update(userInfo);
                            Model.Result = userInfo.pccode;
                            Model.Result1 = userInfo.UserName;
                            Model.Result2 = qyModel.FileServerUrl;
                            Model.Result4 = userInfo;
                        }

                    }
                }

                if (strAction.ToUpper() == "LOGIN")
                {
                    StreamReader sr = new StreamReader(HttpContext.Current.Request.InputStream);
                    string responseStr = sr.ReadToEnd();
                    JObject JsonData = JObject.Parse(responseStr);
                    string username = JsonData["UserName"] == null ? "" : JsonData["UserName"].ToString();
                    string password = JsonData["password"] == null ? "" : JsonData["password"].ToString();
                    string chkcode = JsonData["chkcode"] == null ? "" : JsonData["chkcode"].ToString();
                    Model.ErrorMsg = "";

                    if (chkcode.ToUpper() != "APP")
                    {
                        if (context.Session["chkcode"] != null)
                        {

                            if (!chkcode.ToUpper().Equals(context.Session["chkcode"].ToString()))
                            {
                                Model.ErrorMsg = "验证码不正确";
                            }
                        }
                        else
                        {
                            Model.ErrorMsg = "验证码已过期";

                        }
                    }



                    JH_Auth_QY qyModel = new JH_Auth_QYB().GetALLEntities().First();
                    password = CommonHelp.GetMD5(password);
                    string DataSource = "jh_auth_user";
                    string vssql = "select table_name from information_schema.views where table_name = 'qj_loginuser'";

                    if (DBCon.GetDTByCommand(vssql).Rows.Count > 0)
                    {
                        DataSource = "qj_loginuser";
                    }
                    DataTable userList = DBCon.GetDTByCommand("select * from " + DataSource + " where username=@username and userpass=@userpass", new { username = username, userpass = password });
                    if (userList.Rows.Count == 0)
                    {
                        Model.ErrorMsg = "用户名或密码不正确";
                    }
                    else
                    {
                        if (userList.Rows[0]["IsUse"].ToString() != "Y")
                        {
                            Model.ErrorMsg = "用户被禁用,请联系管理员";
                        }
                        if (Model.ErrorMsg == "")
                        {

                            string strToken = JwtHelper.CreateJWT(username).Token;
                            CacheHelp.Remove(username);//登陆时清理缓存
                            Model.Result = strToken;
                            Model.Result1 = username;
                            JH_Auth_Log log = new JH_Auth_Log()
                            {
                                ComId = "0",
                                LogType = "登录日志",//日志类型(Form删除日志,错误日志)
                                LogContent = context.Request.Path + "/" + Model.Action,//请求头
                                Remark = "系统登录",
                                IP = getIP(context),//IP
                                Remark1 = "",//操作页面
                                CRUser = username + "-" + userList.Rows[0]["UserRealName"].ToString(),//
                                CRDate = DateTime.Now,
                                netcode = "系统登录"
                            };
                            new JH_Auth_LogB().Insert(log);
                        }

                    }



                }

                if (strAction.ToUpper() == "CLEARCACHE")//上传签名
                {
                    CacheHelp.RemoveAll();

                }
                #endregion
            }
            else
            {


            }

            IsoDateTimeConverter timeConverter = new IsoDateTimeConverter();
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
            string Result = JsonConvert.SerializeObject(Model, Newtonsoft.Json.Formatting.Indented, timeConverter).Replace("null", "\"\"");
            context.Response.Write(Result);
        }

        /// <summary>
        /// 成为开发者的第一步，验证并相应服务器的数据
        /// </summary>
        private void Auth(string token, string encodingAESKey, string corpId)
        {

            string echoString = HttpContext.Current.Request.QueryString["echoStr"];
            string signature = HttpContext.Current.Request.QueryString["msg_signature"];//企业号的 msg_signature
            string timestamp = HttpContext.Current.Request.QueryString["timestamp"];
            string nonce = HttpContext.Current.Request.QueryString["nonce"];

            string decryptEchoString = "";
            if (CheckSignature(token, signature, timestamp, nonce, corpId, encodingAESKey, echoString, ref decryptEchoString))
            {
                if (!string.IsNullOrEmpty(decryptEchoString))
                {
                    Int64 v = Convert.ToInt64(decryptEchoString);
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Write(v);
                    HttpContext.Current.Response.End();
                }
            }
        }

        #region 验证企业号签名
        /// <summary>
        /// 验证企业号签名
        /// </summary>
        /// <param name="token">企业号配置的Token</param>
        /// <param name="signature">签名内容</param>
        /// <param name="timestamp">时间戳</param>
        /// <param name="nonce">nonce参数</param>
        /// <param name="corpId">企业号ID标识</param>
        /// <param name="encodingAESKey">加密键</param>
        /// <param name="echostr">内容字符串</param>
        /// <param name="retEchostr">返回的字符串</param>
        /// <returns></returns>
        public bool CheckSignature(string token, string signature, string timestamp, string nonce, string corpId, string encodingAESKey, string echostr, ref string retEchostr)
        {
            return new MOBAPI().CheckSignature(token, signature, timestamp, nonce, corpId, encodingAESKey, echostr, ref retEchostr);
        }

        #endregion
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        public string getIP(HttpContext context)
        {
            string ipAddr = "";
            try
            {
                HttpRequest Request = context.Request;
                // 如果使用代理，获取真实IP  
                if (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
                    ipAddr = Request.ServerVariables["REMOTE_ADDR"];
                else
                    ipAddr = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipAddr == null || ipAddr == "")
                    ipAddr = Request.UserHostAddress;
                return ipAddr;

            }
            catch (Exception ex)
            {
                return "";
            }
            return ipAddr;
        }


    }

}
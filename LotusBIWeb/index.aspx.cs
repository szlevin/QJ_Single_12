﻿using QJY.API;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LotusBIWeb
{
    public partial class index : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            CommonHelp.WriteLOG("获取用户开始");

            string userName = User.Identity.Name;
            CommonHelp.WriteLOG("获取用户" + userName);
            List<JH_Auth_User> userList = new JH_Auth_UserB().GetEntities(d => d.UserName == userName).ToList();
            if (userList.Count > 0)
            {
                string strToken = JwtHelper.CreateJWT(userName).Token;
                CommonHelp.WriteLOG("获取Token" + strToken);

                HttpContext.Current.Response.Cookies.Add(new HttpCookie("username", userName));
                HttpContext.Current.Response.Cookies.Add(new HttpCookie("szhlcode", strToken));
                CommonHelp.WriteLOG("转向页面");

                Response.Redirect("/ViewV5/index.html", false);
            }
            else
            {
                Response.Redirect("/ViewV5/error.html", false);

            }

        }
        //public static String toHexString(String s)
        //{
        //    String str = "";

        //    for (int i = 0; i < s.Length; i++)
        //    {
        //        int ch = (int)s.charAt(i);
        //        String s4 = Integer.toHexString(ch);
        //        str = str + s4;
        //    }
        //    return "0x" + str;//0x表示十六进制  
        //}

        public static FormsAuthenticationTicket GetFormsAuthenticationTicket()
        {
            //Initialize();
            HttpContext context = HttpContext.Current;
            HttpCookie cookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (cookie == null)
            {
                return null;
            }

            if (cookie.Expires != DateTime.MinValue && cookie.Expires < DateTime.Now)
            {
                // ClearAuthCookie();
                return null;
            }

            if (String.IsNullOrEmpty(cookie.Value))
            {
                //ClearAuthCookie();
                return null;
            }

            FormsAuthenticationTicket formsAuthTicket;
            try
            {
                formsAuthTicket = FormsAuthentication.Decrypt(cookie.Value);
            }
            catch
            {
                //ClearAuthCookie();
                return null;
            }

            if (formsAuthTicket == null)
            {
                //ClearAuthCookie();
                return null;
            }

            if (formsAuthTicket.Expired)
            {
                //ClearAuthCookie();
                return null;
            }

            if (String.IsNullOrEmpty(formsAuthTicket.UserData))
            {
                //ClearAuthCookie();
                return null;
            }

            return formsAuthTicket;
        }
    }
}